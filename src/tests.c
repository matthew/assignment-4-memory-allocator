#include "tests.h"

// Help functions

# define create_msg(test_name, msg) "TEST \"%s\": %s\n", test_name, msg


static void debug_heap_message( const void* const heap, const char* const test_name, const char* const msg ) {
    printf( create_msg( test_name, msg ) );
    debug_heap( stdout, heap );
}

static void free_memory(struct test_data* const data) {
    for (size_t i = 0; i < data->malloc_length; ++i) _free( data->malloc[i] );

    munmap( data->heap, size_from_capacity( (block_capacity) {.bytes = data->heap_size} ).bytes );
}

static _Noreturn void test_failed( struct test_data* const data, const char* const test_name, const char* const msg ) {
    free_memory( data );
    err( create_msg( test_name, msg ) );
}

static void allocate_memory( struct test_data* const data, const char* const test_name ) {
    for (size_t i = 0; i < data->malloc_length; ++i) {
        if ( data->malloc[i] ) continue;

        data->malloc[i] = _malloc( data->malloc_size );

        if ( !data->malloc[i] )
            test_failed( data, test_name, "FAILED : Memory isn't allocated" );
    }
}


static void start_test( struct test_data* const data, const char* const test_name ) {
    printf( create_msg( test_name, "START" ) );

    data->heap = heap_init( data->heap_size );

    if ( !data->heap ) err( create_msg( test_name, "FAILED : Heap isn't initialized" ) );
    debug_heap_message( data->heap, test_name, "Heap initialized" );

    allocate_memory( data, test_name );
}

static void end_test( struct test_data* const data, const char* const test_name ) {
    debug_heap_message( data->heap, test_name, "Memory final state" );

    free_memory( data );

    printf( create_msg( test_name, "SUCCESSFUL" ) );
    printf("\n");
}


// *******************************************
// ****************** TESTS ******************
// *******************************************


void run_tests() {
    test_memory_allocate();
    test_free_one_in_several();
    test_free_two_in_several();
    test_allocate_huge_memory();
    test_allocate_another_place_memory();
}


void test_memory_allocate() {
    const char* test_name = "Memory allocate";

    struct test_data data = {
        .heap_size = REGION_MIN_SIZE,
        .malloc_length = 1,
        .malloc_size = REGION_MIN_SIZE / 2
    };

    start_test( &data, test_name );

    debug_heap_message( data.heap, test_name, "Memory allocated" );

    end_test( &data, test_name );
}

void test_free_one_in_several() {
    const char* test_name = "Free one in several";

    struct test_data data = {
        .heap_size = REGION_MIN_SIZE,
        .malloc_length = 3,
        .malloc_size = REGION_MIN_SIZE / 4
    };
    start_test( &data, test_name );

    debug_heap_message( data.heap, test_name, "Memory allocated" );

    _free( data.malloc[1] );

    if ( !data.malloc[0] || !data.malloc[2] )
        test_failed( &data, test_name, "FAILED : Memory damaged" );
    debug_heap_message( data.heap, test_name, "Memory after freeing second block" );

    end_test( &data, test_name );
}

void test_free_two_in_several() {
    const char* test_name = "Free two in several";

    struct test_data data = {
        .heap_size = REGION_MIN_SIZE,
        .malloc_length = 4,
        .malloc_size = REGION_MIN_SIZE / 5
    };
    start_test( &data, test_name );

    debug_heap_message( data.heap, test_name, "Memory allocated" );

    _free( data.malloc[1] );
    _free( data.malloc[2] );

    if ( !data.malloc[0] || !data.malloc[3] )
        test_failed( &data, test_name, "FAILED : Memory damaged" );
    debug_heap_message( data.heap, test_name, "Memory after freeing second and third block" );

    end_test( &data, test_name );
}

void test_allocate_huge_memory() {
    const char* test_name = "Allocate huge memory";

    struct test_data data = {
            .heap_size = REGION_MIN_SIZE,
            .malloc_length = 1,
            .malloc_size = REGION_MIN_SIZE * 2
    };
    start_test( &data, test_name );

    debug_heap_message( data.heap, test_name, "Memory allocated" );

    const struct block_header* header = (struct block_header*) data.heap;
    data.heap_size = size_from_capacity( header->capacity ).bytes;

    if ( data.heap_size < 2 * REGION_MIN_SIZE )
        test_failed( &data, test_name, "FAILED : Heap isn't extended" );

    end_test( &data, test_name );
}

void test_allocate_another_place_memory() {
    const char* test_name = "Allocate memory in another place";

    struct test_data data = {
            .heap_size = REGION_MIN_SIZE,
            .malloc_length = 1,
            .malloc_size = REGION_MIN_SIZE
    };
    start_test( &data, test_name );

    debug_heap_message( data.heap, test_name, "First block allocated" );

    const struct block_header* header = (struct block_header*) (data.malloc[0] - offsetof( struct block_header, contents ));

    void* alloc_region = mmap(
        (void*) (header->contents + header->capacity.bytes),
        // I hate this pipeline and idk why he don't like MAP_ANONYMOUS (0x20)
        REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | 0x20 | MAP_FIXED, -1, 0
    );

    data.malloc_length = 2;
    allocate_memory( &data, test_name );

    debug_heap_message( data.heap, test_name, "Second block allocated" );

    data.heap_size = size_from_capacity( ((struct block_header*) data.heap)->capacity ).bytes;
    const size_t region_size = size_from_capacity( (block_capacity) { REGION_MIN_SIZE } ).bytes;

    end_test( &data, test_name );
    munmap( alloc_region, region_size );
}
